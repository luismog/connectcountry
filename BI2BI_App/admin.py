from django.contrib import admin
from .models.user import User
from .models.categoria import Categoria
from .models.company import Company
from .models.productos import Productos
admin.site.register(User)
admin.site.register(Categoria)
admin.site.register(Company)
admin.site.register(Productos)



# Register your models here.
