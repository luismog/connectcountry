from rest_framework import status,views,validators
from rest_framework.response import Response
from rest_framework.views import APIView
from BI2BI_App.models import Productos
from BI2BI_App.serializers.productosSerializer import ProductosSerializer
from rest_framework.decorators import api_view
from rest_framework import permissions
from rest_framework_simplejwt.backends    import TokenBackend
from rest_framework.permissions           import IsAuthenticated
from django.conf                          import settings



@api_view(['GET', 'POST'])
def products_api_view(request,**kwargs):
    # listar
    if  request.method == 'GET':
        products=[obj for obj in Productos.objects.filter(id_empresa_id=kwargs['id_empresa'] ,
        id_categoria_id=kwargs['id_categoria'])]

        products_serializer = ProductosSerializer(products)
        permission_classes=(IsAuthenticated,)
        token=request.META.get('HTTP_AUTHORIZATION')[7:]
        token_backend=TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data=token_backend.decode(token,verify=False)
        if valid_data['user_id']!=kwargs['id_user']:
            string_response={'detail':'Accedo No Autorizado.'}
            return Response(string_response,status=status.HTTP_401_UNAUTHORIZED)
        return Response(products_serializer.data, status = status.HTTP_200_OK)
        

    # Crear
    elif request.method == 'POST':
        products_serializer = ProductosSerializer(data = request.data)
        permission_classes=(IsAuthenticated,)
        token=request.META.get('HTTP_AUTHORIZATION')[7:] 
        token_backend=TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data=token_backend.decode(token,verify=False)
        if valid_data['user_id']!=kwargs['id_user']:
            string_response={'detail':'Accedo No Autorizado.'}
            return Response(string_response,status=status.HTTP_401_UNAUTHORIZED)
        if products_serializer.is_valid():   
            products_serializer.save()
            string_response={'detail':'Producto creado.'}
            return Response(string_response, status = status.HTTP_200_OK)

        #validacion
        if products_serializer.is_valid():
            products_serializer.save()
            string_response={'detail':'Producto creado.'}
            return Response(string_response, status = status.HTTP_201_CREATED)
        return Response(products_serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def products_detail_view(request,**kwargs):
    #consulta
    products= Productos.objects.get(id_producto = kwargs['id_producto'])
    
    #validacion usuario existe o no existe
    if products:
        # retrieve
        if request.method == 'GET':
            products_serializer = ProductosSerializer(products)
            return Response(products_serializer.data, status = status.HTTP_200_OK)
        # update
        elif request.method == 'PUT':
            products_serializer = ProductosSerializer(products, data = request.data)
            permission_classes=(IsAuthenticated,)
            token=request.META.get('HTTP_AUTHORIZATION')[7:] 
            token_backend=TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
            valid_data=token_backend.decode(token,verify=False)
            if valid_data['user_id']!=kwargs['id_user']:
                string_response={'detail':'Accedo No Autorizado.'}
                return Response(string_response,status=status.HTTP_401_UNAUTHORIZED)
            if products_serializer.is_valid():   
                products_serializer.save()
                string_response={'detail':'Producto actualizado.'}
                return Response(string_response, status = status.HTTP_200_OK)
            return Response(products_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        # delete
        elif request.method == 'DELETE':
            permission_classes=(IsAuthenticated,)
            token=request.META.get('HTTP_AUTHORIZATION')[7:] 
            token_backend=TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
            valid_data=token_backend.decode(token,verify=False)
            if valid_data['user_id']!=kwargs['id_user']:
                string_response={'detail':'Accedo No Autorizado.'}
                return Response(string_response,status=status.HTTP_401_UNAUTHORIZED)
            products.delete()
            return Response({'message': 'Producto Eliminado Correctamente!'}, status = status.HTTP_200_OK)
    # respuesta
    return Response({'message': 'No se ha encontrado un producto con estos datos'}, status = status.HTTP_404_NOT_FOUND)
