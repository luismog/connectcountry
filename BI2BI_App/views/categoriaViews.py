from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from BI2BI_App.models import Categoria
from BI2BI_App.serializers.categoriaSerializer import CategoriaSerializer
from rest_framework.decorators import api_view


@api_view(['GET', 'POST'])
def category_api_view(request):
    # listar
    if request.method == 'GET':
        category = Categoria.objects.all()
        category_serializer = CategoriaSerializer(category, many = True)
        return Response(category_serializer.data, status = status.HTTP_200_OK)


    # Crear
    elif request.method == 'POST':
        category_serializer = CategoriaSerializer(data = request.data)

        #validacion
        if category_serializer.is_valid():
            category_serializer.save()
            return Response(category_serializer.data, status = status.HTTP_201_CREATED)
        return Response(category_serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def category_detail_view(request, pk=None):
    #consulta
    category= Categoria.objects.filter(id = pk).first()
    
    #validacion usuario existe o no existe
    if category:
        # retrieve
        if request.method == 'GET':
            category_serializer = CategoriaSerializer(category)
            return Response(category_serializer.data, status = status.HTTP_200_OK)
        # update
        elif request.method == 'PUT':
            category_serializer = CategoriaSerializer(category, data = request.data)
            if category_serializer.is_valid():
                category_serializer.save()
                return Response(category_serializer.data, status = status.HTTP_200_OK)
            return Response(category_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        # delete
        elif request.method == 'DELETE':
            category.delete()
            return Response({'message': 'Usuario Eliminado Correctamente!'}, status = status.HTTP_200_OK)
    # respuesta
    return Response({'message': 'No se ha encontrado un usuario con estos datos'}, status = status.HTTP_404_NOT_FOUND)