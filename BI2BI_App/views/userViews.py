from rest_framework                       import status
from rest_framework import permissions
from rest_framework.response              import Response
from rest_framework.views                 import APIView
from BI2BI_App.models                     import User
from BI2BI_App.serializers.userSerializer import UserSerializer
from rest_framework.decorators            import api_view
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends    import TokenBackend
from rest_framework.permissions           import IsAuthenticated
from django.conf                          import settings


@api_view(['GET', 'POST'])
def User_api_view(request):
 # listar
    if request.method == 'GET':
        users = User.objects.all()
        users_serializer = UserSerializer(users, many = True)
        return Response(users_serializer.data, status = status.HTTP_200_OK)


    # Crear
    elif request.method == 'POST':
        user_serializer = UserSerializer(data = request.data)

     #validacion
        if user_serializer.is_valid():
            user_serializer.save()
            token_data={"username":request.data['username'],
                            "password":request.data['password']}
            token_serializer=TokenObtainPairSerializer(data=token_data)
            token_serializer.is_valid(raise_exception=True)
            return Response(user_serializer.data and token_serializer.validated_data , 
                    status = status.HTTP_201_CREATED)
        return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

                
            
                    


@api_view(['GET', 'PUT', 'DELETE'])
def user_detail_view(request, pk=None,*args,**kwargs):
     #consulta
    user= User.objects.filter(id = pk).first()
            
      #validacion usuario existe o no existe
    if user:
    # retrieve
        if  request.method == 'GET':
            user_serializer = UserSerializer(user)
            permission_classes=(IsAuthenticated,)
            token=request.META.get('HTTP_AUTHORIZATION')[7:]
            token_backend=TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
            valid_data=token_backend.decode(token,verify=False)
            if valid_data['user_id']!=kwargs['pk']:
                string_response={'detail':'Accedo No Autorizado.'}
                return Response(string_response,status=status.HTTP_401_UNAUTHORIZED)
            return Response(user_serializer.data, status = status.HTTP_200_OK)
        # update
        elif request.method == 'PUT':
            user_serializer = UserSerializer(user, data = request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response(user_serializer.data, status = status.HTTP_200_OK)
            return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        # delete
        elif request.method == 'DELETE':
            user.delete()
            return Response({'message': 'Usuario Eliminado Correctamente!'}, status = status.HTTP_200_OK)
    # respuesta
    return Response({'message': 'No se ha encontrado un usuario con estos datos'}, status = status.HTTP_404_NOT_FOUND)