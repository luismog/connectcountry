from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from BI2BI_App.models import Company
from BI2BI_App.serializers.companySerializer import CompanySerializer
from rest_framework.decorators import api_view


@api_view(['GET', 'POST'])
def company_api_view(request):
    # listar
    if request.method == 'GET':
        company = Company.objects.all()
        products_serializer = CompanySerializer(company, many = True)
        return Response(products_serializer.data, status = status.HTTP_200_OK)


    # Crear
    elif request.method == 'POST':
        products_serializer = CompanySerializer(data = request.data)

        #validacion
        if products_serializer.is_valid():
            products_serializer.save()
            return Response(products_serializer.data, status = status.HTTP_201_CREATED)
        return Response(products_serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def company_detail_view(request, pk=None):
    #consulta
    company= Company.objects.filter(id = pk).first()
    company_id = User.objects.get('id')
    
    #validacion usuario existe o no existe
    if company:
        # retrieve
        if request.method == 'GET':
            company_serializer = CompanySerializer(company)
            return Response(company_serializer.data, status = status.HTTP_200_OK)
        # update
        elif request.method == 'PUT':
            company_serializer = CompanySerializer(company, data = request.data)
            if company_serializer.is_valid():
                company_serializer.save()
                return Response(company_serializer.data, status = status.HTTP_200_OK)
            return Response(company_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        # delete
        elif request.method == 'DELETE':
            company.delete()
            return Response({'message': 'Usuario Eliminado Correctamente!'}, status = status.HTTP_200_OK)
    # respuesta
    return Response({'message': 'No se ha encontrado un usuario con estos datos'}, status = status.HTTP_404_NOT_FOUND)