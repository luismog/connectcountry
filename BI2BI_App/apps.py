from django.apps import AppConfig


class Bi2BiAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'BI2BI_App'
