from django.db import models

class Categoria(models.Model):
    id_categoria=models.BigAutoField(primary_key=True)
    nombre_categoria=models.CharField('Categoria', max_length =10 ,null=False)
