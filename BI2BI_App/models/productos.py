from django.db import models
from .company import Company
from .categoria import Categoria



class Productos(models.Model):
    id_producto=models.BigAutoField(primary_key=True)
    codigo_producto_empresa=models.CharField('Codigo producto',max_length=10, default = "asda", null=True, unique=True)
    id_empresa=models.ForeignKey(Company,related_name='producto' ,on_delete=models.CASCADE)
    nombre_producto=models.CharField('Nombre producto',max_length=20,null=False, unique=True)
    descripcion_producto=models.TextField('Descripción',max_length=200,null=True)
    precio=models.DecimalField('Precio',max_digits=10,decimal_places=2)
    link_foto=models.URLField('Foto',default = "https://image.shutterstock.com/image-vector/picture-vector-icon-no-image-600w-1350441335.jpg" , null = True) 
    id_categoria=models.ForeignKey(Categoria,related_name='categoria',on_delete=models.PROTECT)
        

    


