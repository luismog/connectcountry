from django.db import models
from .user import User

class Company (models.Model):
    id_empresa=models.BigAutoField(primary_key=True)
    id_user=models.ForeignKey(User,related_name='company', on_delete=models.CASCADE)
    name_company=models.CharField('Name company', max_length = 70, unique=True,null=False)

