from django.urls import path
from BI2BI_App.views.categoriaViews import category_api_view, category_detail_view
#from BI2BI_App.serializers.userSerializer import to_representation

urlpatterns = [
    path('categoria', category_api_view, name = 'usuario_api'),
    path('categoria/<int:pk>', category_detail_view, name = 'category_detail_view')
    ] 