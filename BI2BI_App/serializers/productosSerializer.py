
from rest_framework import serializers
from BI2BI_App.models.productos import Productos
from BI2BI_App.models.categoria import Categoria


class ProductosSerializer(serializers.ModelSerializer):
    class Meta:
         model = Productos
         fields =[ 'codigo_producto_empresa','id_empresa','nombre_producto','descripcion_producto',
         'precio','link_foto' ,'id_categoria']


    def to_representation(self,lista,**kwargs):
          productos={}
          for producto in lista:
              listaProductos={
               "id_producto #"+str(producto.id_producto):{
               "codigo_producto_empresa"            : producto.codigo_producto_empresa,
               "nombre_producto"                     : producto.nombre_producto ,
               "descripcion_producto"                : producto.descripcion_producto,
               "precio"                              : producto.precio ,
               "link_foto"                           : producto.link_foto,
              }}
              productos.update(listaProductos)
          return productos