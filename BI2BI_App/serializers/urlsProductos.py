from django.urls import path
from BI2BI_App.views.productosViews import products_api_view, products_detail_view
#from BI2BI_App.serializers.userSerializer import to_representation

urlpatterns = [
    path('crear/<int:id_user>/', products_api_view, name = 'crear_producto'),
    path('listar/<int:id_user>/<int:id_empresa>/<int:id_categoria>', products_api_view, name = 'listar_producto_segun_categoria'),
    path('eliminar/<int:id_user>/<int:id_producto>', products_detail_view, name = 'borrar producto'),
    path('actualizar/<int:id_user>/<str:id_producto>/', products_detail_view, name = 'Actualizar_producto_segun_codigo_empresa_producto')
    ] 