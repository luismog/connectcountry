from django.urls import path
from BI2BI_App.views.companyViews import company_api_view, company_detail_view


urlpatterns = [
    path('company', company_api_view, name = 'usuario_api'),
    path('company/<int:pk>', company_detail_view, name = 'company_detail_view')
    ] 