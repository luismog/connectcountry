from django.urls import path
from BI2BI_App.views.userViews import User_api_view, user_detail_view




urlpatterns = [
    path('crear/', User_api_view, name = 'usuario_api'),
    path('usuario/<int:pk>', user_detail_view, name = 'usuario_detail_view'),
    
    
    ] 