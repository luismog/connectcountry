from django.http.response import FileResponse
from BI2BI_App.models.categoria import Categoria
from rest_framework import serializers


class CategoriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Categoria
        fields = "__all__"

