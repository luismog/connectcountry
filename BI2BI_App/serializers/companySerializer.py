from rest_framework import serializers
from BI2BI_App.models.company import Company



class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['name_company']
    
  
   