from rest_framework                          import serializers
from BI2BI_App.models.user                   import User
from BI2BI_App.models.company                import Company
from BI2BI_App.serializers.companySerializer import CompanySerializer



class UserSerializer(serializers.ModelSerializer):
  company=CompanySerializer()
  class Meta:
     model = User
     fields = ['username' ,'password','name' ,'email','tipo_documento','identificacion',
    'id_ciudad','numero_contacto','company']
  def create(self,validated_data):
    companyData=validated_data.pop('company')
    userInstance=User.objects.create(**validated_data)
    Company.objects.create(id_user=userInstance,**companyData)
    return userInstance

  def to_representation(self, instance):
    usuario=User.objects.get(id=instance.id)
    company=Company.objects.get(id_user=instance.id)
    return {
      "id"            : usuario.id,
      "username"       : usuario.username ,
      "password"       :usuario.password,
      "name"           : usuario.name ,
      "email"          : usuario.email,
      "tipo_documento" : usuario.tipo_documento,
      "identificacion" : usuario.identificacion,
      "id_ciudad"      : usuario.id_ciudad,
      "numero_contacto":usuario.numero_contacto,
      "company":{
        "name_company":company.name_company,
        
        } 
        }

 

